package com.kleshchenko.comparator;

import com.kleshchenko.model.OldCard;
import java.util.Comparator;

public class CardComparator implements Comparator<OldCard> {
    @Override
    public int compare(OldCard o1, OldCard o2) {
        return Double.compare(o1.getChars().getAlcFraction(), o2.getChars().getAlcFraction());
    }
}
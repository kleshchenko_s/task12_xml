package com.kleshchenko.model;

import java.util.List;

public class OldCard {
    private String theme;
    private String type;
    private boolean sent;
    private String country;
    private int year;
    private String author;
    private String valuable;

    OldCard() {}

    OldCard(String theme, String type, boolean sent, String country, int year, String author, String valuable){
        this.theme = theme;
        this.type = type;
        this.sent = sent;
        this.country = country;
        this.year = year;
        this.author = author;
        this.valuable = valuable;
    }

    public String getTheme() {
        return theme;
    }

    public String getType() {
        return type;
    }

    public boolean isSent() {
        return sent;
    }

    public String getCountry() {
        return country;
    }

    public int getYear() { return year; }

    public String getAuthor() {
        return author;
    }

    public String getValuable() {
        return valuable;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setValuable(String valuable) {
        this.valuable = valuable;
    }

    @Override
    public String toString() {
        return "Card{" +
                ", theme='" + theme + '\'' +
                ", type='" + type + '\'' +
                ", sent=" + sent +
                ", year='" + year + '\'' +
                ", author=" + author +
                ", valuable=" + valuable +
                '}';
    }
}

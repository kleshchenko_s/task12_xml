<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
<xsl:template match="/">
    <html>
        <head>
            <style type="text/css">
                table.tfmt {
                border: 1px ;
                }

                td.colfmt {
                border: 1px ;
                background-color: yellow;
                color: red;
                text-align:right;
                }

                th {
                background-color: #2E9AFE;
                color: white;
                }

            </style>
        </head>

        <body>
            <table class="tfmt">
                <tr>
                    <th style="width:250px">theme</th>
                    <th style="width:350px">type</th>
                    <th style="width:350px">sent</th>
                    <th style="width:250px">country</th>
                    <th style="width:250px">year</th>
                    <th style="width:250px">author</th>
                    <th style="width:250px">valuable</th>
                </tr>

                <xsl:for-each select="old_cards/old_card">

                    <tr>

                        <td class="colfmt">
                            <xsl:value-of select="theme" />
                        </td>
                        <td class="colfmt">
                            <xsl:value-of select="type" />
                        </td>
                        <td class="colfmt">
                            <xsl:value-of select="sent" />
                        </td>
                        <td class="colfmt">
                            <xsl:value-of select="country" />
                        </td>
                        <td class="colfmt">
                            <xsl:value-of select="year" />
                        </td>
                        <td class="colfmt">
                            <xsl:value-of select="author" />
                        </td>
                        <td class="colfmt">
                            <xsl:value-of select="valuable" />
                        </td>
                    </tr>

                </xsl:for-each>
            </table>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>